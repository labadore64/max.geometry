﻿using max.serialize;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace max.geometry.shape2D
{
    /// <summary>
    /// This class represents a line on a 2D plane.
    /// </summary>
    public class Line2D : Geometry2D
    {
        #region Constants
        // strings used for serialization key names
        const string SER_POINTS = "points";

        // epsilon
        const float EPSILON = 1f;

        #endregion

        #region Properties

        /// <summary>
        /// The 2 points of the line.
        /// </summary>
        /// <value>Points</value>
        public override Vector2[] Points { get; protected set; } = new Vector2[2] { Vector2.Zero, Vector2.Zero };

        /// <summary>
        /// The distance of the line.
        /// </summary>
        /// <value>Distance</value>
        public float Distance
        {
            get
            {
                return Vector2.Distance(Points[0], Points[1]);
            }
        }

        public override Vector2 Center
        {
            get
            {
                return _center;
            }
        }

        Vector2 _center;

        /// <summary>
        /// Returns the starting point.
        /// </summary>
        /// <value>Start point</value>
        public Vector2 StartPoint
        {
            get
            {
                return Points[0];
            }
            set
            {
                Points[0] = value;
                _center = new Vector2((float)((Points[0].X + Points[1].X) * .5), (float)((Points[0].Y + Points[1].Y) * .5));
            }
        }

        /// <summary>
        /// Returns the ending point.
        /// </summary>
        /// <value>End point</value>
        public Vector2 EndPoint
        {
            get
            {
                return Points[1];
            }
            set
            {
                Points[1] = value;
                _center = new Vector2((float)((Points[0].X + Points[1].X) * .5), (float)((Points[0].Y + Points[1].Y) * .5));
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a line from 2 points.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        public Line2D(Vector2 Point1, Vector2 Point2)
        {
            Points[0] = Point1;
            Points[1] = Point2;
            _center = new Vector2((float)((Points[0].X + Points[1].X) * .5), (float)((Points[0].Y + Points[1].Y) * .5));
        }


        /// <summary>
        /// Creates a line from an array of at least 2 points.
        /// </summary>
        /// <param name="Points">The points</param>
        public Line2D(Vector2[] Points)
        {
            this.Points[0] = Points[0];
            this.Points[1] = Points[1];
            _center = new Vector2((float)((Points[0].X + Points[1].X) * .5), (float)((Points[0].Y + Points[1].Y) * .5));
        }

        /// <summary>
        /// Creates a Line with the same properties as another line.
        /// </summary>
        /// <param name="Line">The line</param>
        public Line2D(Line2D Line)
        {
            Points[0] = Line.Points[0];
            Points[1] = Line.Points[1];
            _center = new Vector2((float)((Points[0].X + Points[1].X) * .5), (float)((Points[0].Y + Points[1].Y) * .5));
        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public Line2D()
        {
        }

        #endregion

        #region PointInGeometry

        /// <summary>
        /// Returns whether or not a certain point is on a line.
        /// </summary>
        /// <param name="TestPoint">Point to test.</param>
        /// <returns>True/False</returns>
        public override bool PointInGeometry(Vector2 TestPoint)
        {
            return PointInGeometry(Points[0], Points[1], TestPoint);
        }

        /// <summary>
        /// Returns whether or not a certain point is on a line.
        /// </summary>
        /// <param name="TestPoint">Point to test</param>
        /// <param name="Points">The points of the line</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(Vector2[] Points, Vector2 TestPoint)
        {
            return PointInGeometry(Points[0], Points[1], TestPoint);
        }

        /// <summary>
        /// Returns whether or not a certain point is on a line.
        /// </summary>
        /// <param name="TestPoint">Point to test</param>
        /// <param name="Line">The line</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(Line2D Line, Vector2 TestPoint)
        {
            return PointInGeometry(Line.Points, TestPoint);
        }

        /// <summary>
        /// Returns whether or not a certain point is on a line.
        /// </summary>
        /// <param name="TestPoint">Point to test.</param>
        /// <param name="Point1">The first point on the line.</param>
        /// <param name="Point2">The second point on the line.</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(Vector2 Point1, Vector2 Point2, Vector2 TestPoint)
        {
            float dxl = Point2.X - Point1.X;
            float dyl = Point2.Y - Point1.Y;

            if (Math.Abs(dxl) >= Math.Abs(dyl))
            {
                return dxl > 0 ?
                  Point1.X <= TestPoint.X && TestPoint.X <= Point2.X :
                  Point2.X <= TestPoint.X && TestPoint.X <= Point1.X;
            }
            else
            {
                return dyl > 0 ?
                  Point1.Y <= TestPoint.Y && TestPoint.Y <= Point2.Y :
                  Point2.Y <= TestPoint.Y && TestPoint.Y <= Point1.Y;
            }
        }

        #endregion

        #region PointAlongLinePercent

        /// <summary>
        /// Get a point that is a certain percentage along the line. For example, to get the midpoint,
        /// the Percent value would be .5. If the percent value is out of range it is clamped between 0-1.
        /// </summary>
        /// <param name="Percent">Value 0-1 representing point along line</param>
        /// <returns>Point</returns>
        public Vector2 PointAlongLinePercent(float Percent)
        {
            return PointAlongLinePercent(Points[0], Points[1], Percent);
        }

        /// <summary>
        /// Get a point that is a certain percentage along the line. For example, to get the midpoint,
        /// the Percent value would be .5. If the percent value is out of range it is clamped between 0-1.
        /// </summary>
        /// <param name="Line">The line</param>
        /// <param name="Percent">Value 0-1 representing point along line</param>
        /// <returns>Point</returns>
        public static Vector2 PointAlongLinePercent(Line2D Line, float Percent)
        {
            return PointAlongLinePercent(Line.Points[0], Line.Points[1], Percent);
        }

        /// <summary>
        /// Get a point that is a certain percentage along the line. For example, to get the midpoint,
        /// the Percent value would be .5. If the percent value is out of range it is clamped between 0-1.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <param name="Percent">Value 0-1 representing point along line</param>
        /// <returns>Point</returns>
        public static Vector2 PointAlongLinePercent(Vector2 Point1, Vector2 Point2, float Percent)
        {
            if(Percent > 1)
            {
                Percent = 1;
            } else if (Percent < 0)
            {
                Percent = 0;
            }

            //gets the dist from the first point to go
            float dist = Vector2.Distance(Point1,Point2) * Percent;

            return PointAlongLine(Point1, Point2, dist);
        }

        #endregion

        #region PointAlongLine

        /// <summary>
        /// Get a point that is a certain distance along the line. For example, to get the midpoint,
        /// the Distance value would be half the distance between Line.Points[0] and Line.Points[1]. If the distance is
        /// out of range it will be clamped between 0-(Distance between Line.Points[0] and Line.Points[1]).
        /// </summary>
        /// <param name="Distance">Distance along the line.</param>
        /// <returns>Point</returns>
        public Vector2 PointAlongLine(float Distance)
        {
            return PointAlongLine(Points[0], Points[1], Distance);
        }

        /// <summary>
        /// Get a point that is a certain distance along the line. For example, to get the midpoint,
        /// the Distance value would be half the distance between Line.Points[0] and Line.Points[1]. If the distance is
        /// out of range it will be clamped between 0-(Distance between Line.Points[0] and Line.Points[1]).
        /// </summary>
        /// <param name="Line">The line</param>
        /// <param name="Distance">Distance along the line.</param>
        /// <returns>Point</returns>
        public static Vector2 PointAlongLine(Line2D Line, float Distance)
        {
            return PointAlongLine(Line.Points[0], Line.Points[1], Distance);
        }

        /// <summary>
        /// Get a point that is a certain distance along the line. For example, to get the midpoint,
        /// the Distance value would be half the distance between Point1 and Point2. If the distance is
        /// out of range it will be clamped between 0-(Distance between Point1 and Point2).
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <param name="Distance">Distance along the line.</param>
        /// <returns>Point</returns>
        public static Vector2 PointAlongLine(Vector2 Point1, Vector2 Point2, float Distance)
        {
            float dist = Vector2.Distance(Point1, Point2);
            if(Distance > dist)
            {
                Distance = dist;
            } else if (Distance < 0)
            {
                Distance = 0;
            }

            Vector2 DestinationDirection = Point2 - Point1;

            //gets the angle of the line
            double angle = Math.Atan2(DestinationDirection.Y, DestinationDirection.X);

            //returns the first point + the distance along the angle
            return new Vector2(
                (float)(Point1.X + Math.Cos(angle) * Distance),
                (float)(Point1.Y + Math.Sin(angle) * Distance)
            );
        }
        #endregion

        #region Copy
        /// <summary>
        /// Copies this Line as an object.
        /// </summary>
        /// <returns>Copied shape</returns>
        public override I2DGeometry Copy()
        {
            return new Line2D(this);
        }

        /// <summary>
        /// Returns a copy of the source line.
        /// </summary>
        /// <param name="Source">Source line</param>
        /// <returns>Copied line</returns>
        public static Line2D Copy(Line2D Source)
        {
            return (Line2D)Source.Copy();
        }
        #endregion

        #region IntersectPoints
        /// <summary>
        /// Returns the points that intersect this shape with another.
        /// </summary>
        /// <param name="Shape">Shape to intersect</param>
        /// <returns>Array of intersection points</returns>
        public override List<Vector2> IntersectPoints(I2DGeometry Shape)
        {
            if (Shape is Path2D)
            {
                Shape = ((Path2D)Shape).ToPolygon2D();
            }

            // if shape is a polygon
            if (Shape is Polygon2D)
            {
                Polygon2D Polygon = (Polygon2D)Shape;
                Line2D[] Lines = Polygon.Lines;
                return IntersectPolygon2D(Points[0], Points[1], Lines);
            }
            // if shape is a circle
            else if (Shape is Circle2D)
            {
                Circle2D circle = (Circle2D)Shape;
                return Circle2D.IntersectLine2D(circle.Center, circle.Radius, Points[0], Points[1]);
            }
            // if shape is a line
            else if (Shape is Line2D)
            {
                return IntersectLine2D(Points[0], Points[1], Shape.Points[0], Shape.Points[1]);
            }

            // if all else fails just return an empty array
            return new List<Vector2>();
        }

        /// <summary>
        /// Returns the points where a line and a circle intersect.
        /// </summary>
        /// <param name="Point1">The first point of the line</param>
        /// <param name="Point2">The second point of the line</param>
        /// <param name="Circle">The circle</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectCircle2D(Vector2 Point1, Vector2 Point2, Circle2D Circle)
        {
            return Circle2D.IntersectLine2D(Circle.Center, Circle.Radius, Point1, Point2);
        }

        /// <summary>
        /// Returns the points where a line and a circle intersect.
        /// </summary>
        /// <param name="Point1">The first point of the line</param>
        /// <param name="Point2">The second point of the line</param>
        /// <param name="Center">The center of the circle</param>
        /// <param name="Radius">The radius of the circle</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectCircle2D(Vector2 Point1, Vector2 Point2, Vector2 Center, float Radius)
        {
            return Circle2D.IntersectLine2D(Center, Radius, Point1, Point2);
        }

        /// <summary>
        /// Returns the point where two lines intersect.
        /// </summary>
        /// <param name="Point1">First point of first line</param>
        /// <param name="Point2">Second point of first line</param>
        /// <param name="Point3">First point of second line</param>
        /// <param name="Point4">Second point of second line</param>
        /// <returns>The intersection point</returns>
        public static List<Vector2> IntersectLine2D(Vector2 Point1, Vector2 Point2, Vector2 Point3, Vector2 Point4)
        {
            float xx = float.MinValue;
            float yy = float.MinValue;

            float A1 = Point2.Y - Point1.Y;
            float B1 = Point1.X - Point2.X;
            float C1 = A1 * Point1.X + B1 * Point1.Y;

            float A2 = Point4.Y - Point3.Y;
            float B2 = Point3.X - Point4.X;
            float C2 = A2 * Point3.X + B2 * Point3.Y;

            float det = A1 * B2 - A2 * B1;

            if (Math.Abs(det) < EPSILON)
            {
                //parallel
            }
            else
            {
                float detter = 1 / det;
                xx = ((B2 * C1 - B1 * C2) * detter);
                yy = ((A1 * C2 - A2 * C1) * detter);

            }

            Vector2 ReturnVec = new Vector2(xx, yy);

            if (PointInGeometry(Point3, Point4, ReturnVec) && PointInGeometry(Point1, Point2, ReturnVec))
            {
                List<Vector2> returner = new List<Vector2>();
                returner.Add(ReturnVec);
                return returner;
            }

            return new List<Vector2>();
        }

        /// <summary>
        /// Returns the point where a line and a polygon intersect.
        /// </summary>
        /// <param name="Point1">First point of first line</param>
        /// <param name="Point2">Second point of first line</param>
        /// <param name="Lines">Lines of the polygon</param>
        /// <returns>The intersection point</returns>
        public static List<Vector2> IntersectPolygon2D(Vector2 Point1, Vector2 Point2, Line2D[] Lines)
        {
            List<List<Vector2>> LineCollisions = new List<List<Vector2>>();

            for (int i = 0; i < Lines.Length; i++)
            {
                LineCollisions.Add(IntersectLine2D(Point1, Point2, Lines[0].Points[0], Lines[0].Points[1]));
            }

            // put all the results together in one array

            List<Vector2> Results = new List<Vector2>();

            for (int i = 0; i < LineCollisions.Count; i++)
            {
                for (int j = 0; j < LineCollisions[i].Count; j++)
                {
                    Results.Add(LineCollisions[i][j]);
                }
            }

            return Results;
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the line into a Dictionary.
        /// </summary>
        /// <returns>Serialized object</returns>
        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject data = MaxSerializedObject.Create();
            data.SetType(this);
            data.AddVector2Collection(SER_POINTS, Points);
            return data;
        }

        /// <summary>
        /// Sets data in a Dictionary to this Line.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {

            Points = Data.GetVector2List(SER_POINTS).ToArray();
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new Line2D();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
