﻿using System;
using System.Collections.Generic;
using max.serialize;
using max.serialize.type;
using Microsoft.Xna.Framework;

namespace max.geometry.shape2D
{
    /// <summary>
    /// This class represents a path in 2D space.
    /// 
    /// A path has a point that is connected by a bunch of lines.
    /// 
    /// </summary>
    public class Path2D : Geometry2D
    {
        #region Constants

        private const float EPSILON = .001f;

        const string SER_POINTS = "points";
        const string SER_ENDSTATE = "endstate";

        #endregion

        #region Private Variables

        // how much distance was travelled on this line 
        float thisTravelDistance = 0;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the center of the Path.
        /// </summary>
        /// <value>Center</value>
        public override Vector2 Center 
        { 
            get
            {
                return ToPolygon2D().Center;
            }
        }

        /// <summary>The speed to move along.</summary>
        /// <value>Speed</value>
        public float Speed { get; set; } = 1;

        /// <summary>Whether or not the path is in motion.</summary>
        /// <value>In motion</value>
        public bool Started { get; protected set; }

        /// <summary>The distance of the entire path.</summary>
        /// <value>Distance</value>
        public float Distance { get; protected set; }

        /// <summary>The distance along the path travelled.</summary>
        /// <value>Distance</value>
        public float DistanceTravelled { get; protected set; }

        /// <summary>
        /// The current position along the path.
        /// </summary>
        /// <value>Position</value>
        public Vector2 Position { get; protected set; }

        /// <summary>
        /// The index of the current line in the path being travelled on.
        /// </summary>
        /// <value>Index</value>
        public int Index { get; protected set; }

        /// <summary>
        /// The points of the path.
        /// </summary>
        /// <value>Path points</value>
        public override Vector2[] Points { get; protected set; }

        /// <summary>
        /// The lines of the looped path.
        /// </summary>
        /// <value>Path lines</value>
        public Line2D[] Lines {
            get
            {
                return _lines;
            }
            protected set
            {
                _lines = value;
                UpdateDistance();
            }
        }
        // part of Lines
        Line2D[] _lines;

        /// <summary>
        /// The current line in the path being travelled on.
        /// </summary>
        /// <value>Line</value>
        public Line2D CurrentLine { get { return Lines[Index]; } }
        
        /// <summary>
        /// The end behavior of the path when running.
        /// </summary>
        public enum EndBehavior {
            ///<summary>Loops from the last point to the first point.</summary>
            LOOP,
            ///<summary>Stops the path.</summary>
            STOP,
            ///<summary>Moves instantly to the first point and loops.</summary>
            RESTART
        };

        /// <summary>
        /// The chosen end behavior of the path.
        /// </summary>
        /// <value>End Behavior</value>
        public EndBehavior PathEndBehavior
        {
            get
            {
                return _endState;
            }
            set
            {
                _endState = value;
                UpdateDistance();
            }
        }

        EndBehavior _endState = EndBehavior.LOOP;

        #endregion

        #region Constructors

        /// <summary>
        /// Initialize using a path represented as points.
        /// </summary>
        /// <param name="Points">Points</param>
        public Path2D(IList<Vector2> Points)
        {
            List<Line2D> lines = new List<Line2D>();
            for(int i = 0; i < Points.Count; i++)
            {
                // adds a line with the points Point[i] and Point[i+1] (but mod adjusted
                // so it doesn't have an array out of bounds error and loops back to the first value
                lines.Add(new Line2D(Points[i], Points[(i + 1) % Points.Count]));
            }

            Lines = lines.ToArray();
            Position = CurrentLine.Points[0];
            this.Points = new Vector2[Points.Count];
            Points.CopyTo(this.Points, 0);
        }

        /// <summary>
        /// Initialize using a path represented as points.
        /// </summary>
        /// <param name="Points">Points</param>
        public Path2D(Vector2[] Points)
        {
            List<Line2D> lines = new List<Line2D>();
            for (int i = 0; i < Points.Length; i++)
            {
                // adds a line with the points Point[i] and Point[i+1] (but mod adjusted
                // so it doesn't have an array out of bounds error and loops back to the first value
                lines.Add(new Line2D(Points[i], Points[(i + 1) % Points.Length]));
            }

            Lines = lines.ToArray();
            Position = CurrentLine.Points[0];
            this.Points = Points;
        }

        /// <summary>
        /// Initialize using a path using another path.
        /// </summary>
        /// <param name="Path">Path</param>
        public Path2D(Path2D Path)
        {
            List<Line2D> lines = new List<Line2D>();
            for (int i = 0; i < Path.Points.Length; i++)
            {
                // adds a line with the points Point[i] and Point[i+1] (but mod adjusted
                // so it doesn't have an array out of bounds error and loops back to the first value
                lines.Add(new Line2D(Path.Points[i], Path.Points[(i + 1) % Path.Points.Length]));
            }

            Lines = lines.ToArray();
            Position = CurrentLine.Points[0];
            Points = Path.Points;
        }

        /// <summary>
        /// Initialize using a path using a Polygon.
        /// </summary>
        /// <param name="Polygon">Polygon</param>
        public Path2D(Polygon2D Polygon)
        {
            Lines = Polygon.Lines;
            Position = CurrentLine.Points[0];
            Points = Polygon.Points;
        }

        /// <summary>
        /// Initialize using serialized path data.
        /// </summary>
        /// <param name="Data">Data</param>
        public Path2D(MaxSerializedObject Data)
        {
            Deserialize(Data);
        }

        /// <summary>
        /// Initialize using serialized path data.
        /// </summary>
        /// <param name="Data">Data</param>
        public Path2D (object Data)
        {
            Deserialize((MaxSerializedObject)Data);
        }

        /// <summary>
        /// Empty constructor.
        /// </summary>
        public Path2D()
        {

        }

        #endregion

        #region Control Methods
        /// <summary>
        /// Updates the state of the path for a cycle.
        /// </summary>
        public void Update()
        {
            if (Started)
            {
                MoveForward(Speed);
            }
        }

        /// <summary>
        /// Updates the state of the path for a cycle in reverse.
        /// </summary>
        public void UpdateReverse()
        {
            if (Started)
            {
                MoveBackward(Speed);
            }
        }


        /// <summary>
        /// Starts the path at the beginning.
        /// </summary>
        public void Start()
        {
            ResetPosition();
            Started = true;
        }

        /// <summary>
        /// Starts the path at a certain distance along the path.
        /// </summary>
        /// <param name="Distance">Distance along the path</param>
        public void Start(float Distance)
        {
            SetPosition(Distance);
            Started = true;
        }

        #endregion

        #region Internal methods

        // updates the distance of the entire path.
        // this method is called when you update the path or
        // its end behavior
        private void UpdateDistance()
        {
            int leng = Lines.Length;
            if (PathEndBehavior != EndBehavior.LOOP)
            {
                leng--;
            }
            Distance = 0;
                for (int i = 0; i < leng; i++)
                {
                    Distance += Lines[i].Distance;
                }
        }

        #endregion

        #region Non-Static methods
        // These methods can't be used statically so they are added normally.
        
        /// <summary>
        /// Moves the path forward a set distance.
        /// </summary>
        /// <param name="Distance">Distance to move</param>
        public void MoveForward(float Distance)
        {
            thisTravelDistance += Distance;

            // if the distance on this line is longer than the
            // actual line distance...
            if(thisTravelDistance > CurrentLine.Distance)
            {
                // move to next line

                // get difference between the two lines
                float diff = thisTravelDistance - CurrentLine.Distance;
                Index++;

                // if last or second-to-last index, check the state based on the index.
                if (Index >= Lines.Length - 2)
                {
                    // if second to last index
                    if (Index == Lines.Length - 1)
                    {
                        // check if state is restart. if true, reset.
                        if (PathEndBehavior == EndBehavior.RESTART)
                        {
                            Index = 0;
                            DistanceTravelled = diff;
                        } 
                        // Uf the state is stop, stop the path.
                        else if (PathEndBehavior == EndBehavior.STOP)
                        {
                            Started = false;
                        }

                    }
                    // otherwise if last index
                    else if (Index == Lines.Length)
                    {
                        // check if state is restart. if true, reset.
                        // if false, end.
                        if (PathEndBehavior == EndBehavior.LOOP || PathEndBehavior == EndBehavior.RESTART)
                        {
                            Index = 0;
                            DistanceTravelled = diff;
                        }
                        else
                        {
                            Started = false;
                        }
                    }
                }

                thisTravelDistance = diff;
            }

            // set the position based on the distance along the current line
            Position = CurrentLine.PointAlongLine(thisTravelDistance);
        }

        /// <summary>
        /// Moves the path backward a set distance.
        /// </summary>
        /// <param name="Distance">Distance to move</param>
        public void MoveBackward(float Distance)
        {
            thisTravelDistance -= Distance;

            // if the distance on this line is longer than the
            // actual line distance...
            if (thisTravelDistance < 0)
            {
                // move to previous line
                Index--;

                // if the index is less than or equal to 1,
                // this is end behavior
                if(Index < 0)
                {
                    // if end state is loop, go to last line
                    if (PathEndBehavior == EndBehavior.LOOP)
                    {
                        Index = Lines.Length - 1;
                    }
                    // if end state is restart, go to second to last line
                    else if (PathEndBehavior == EndBehavior.RESTART)
                    {
                        Index = Lines.Length - 2;
                    }
                    // otherwise end the path
                    else
                    {
                        Started = false;
                    }
  
                }

                // Add the negative distance to the new distance of the 
                // last line.
                float diff = thisTravelDistance + CurrentLine.Distance;

                thisTravelDistance = diff;
            }

            // set the position based on the distance along the current line
            Position = CurrentLine.PointAlongLine(thisTravelDistance);
        }

        /// <summary>
        /// Sets the path to be a certain distance along the path from the first point
        /// of the first line.
        /// </summary>
        /// <param name="Distance">Distance</param>
        public void SetPosition(float Distance)
        {
            // first reset the position
            ResetPosition();

            // make sure distance is between 0-this.Distance
            float dist = this.Distance;
            if (Distance > dist)
            {
                Distance = dist;
            } else if (Distance < 0)
            {
                Distance = 0;
            }

            float _movedDistance = Distance;
            float _lineDist;

            // This loop iterates through the lines until the
            // variable _movedDistance is below 0 or until it
            // reaches the last index
            while(_movedDistance > 0 && Index < Lines.Length)
            {
                // gets the distance of the current line
                _lineDist = Lines[Index].Distance;

                // if the _movedDistance is still larger than the length
                // of the current line, move to the next line.
                if (_movedDistance > _lineDist)
                {
                    // deduct distance of current line from _movedDistance
                    _movedDistance -= _lineDist;

                    // if there is still distance to traverse, increase the 
                    // index. (otherwise it will just exit when done)
                    if (_movedDistance > 0)
                    {
                        Index++;
                    }
                } else
                {
                    break;
                }
            }

            Position = Lines[Index].PointAlongLine(_movedDistance);

            thisTravelDistance = Distance;
        }

        /// <summary>
        /// Set the position as a percent along the line. For example,
        /// to set the position to the midpoint, use .5.
        /// </summary>
        /// <param name="Percent">Value 0-1</param>
        public void SetPositionPercent(float Percent)
        {
            
            if(Percent > 1)
            {
                Percent = 1;
            } else if (Percent < 0)
            {
                Percent = 0;
            }

            float dist = Distance * Percent;

            SetPosition(dist);
        }

        /// <summary>
        /// Set the position as the starting point of the Line at the selected index.
        /// </summary>
        /// <param name="Index">Index of line to start from</param>
        public void SetPosition(int Index)
        {
            this.Index = Index;
            Position = Lines[Index].Points[0];
        }

        /// <summary>
        /// Resets the position of the path to the beginning.
        /// </summary>
        public void ResetPosition()
        {
            SetPosition(0);
        }

        /// <summary>
        /// Returns a Polygon2D from the path points.
        /// </summary>
        /// <returns>Polygon2D</returns>
        public Polygon2D ToPolygon2D()
        {
            return new Polygon2D(Points);
        }

        #endregion

        #region Copy

        /// <summary>
        /// Copies this Path as an object.
        /// </summary>
        /// <returns>Copied shape</returns>
        public override I2DGeometry Copy()
        {
            return new Path2D(this);
        }

        /// <summary>
        /// Returns a copy of the source path.
        /// </summary>
        /// <param name="Source">Source path</param>
        /// <returns>Copied path</returns>
        public static Path2D Copy(Path2D Source)
        {
            return (Path2D)Source.Copy();
        }

        #endregion

        #region IntersectPoints
        /// <summary>
        /// Returns the points that intersect this shape with another.
        /// </summary>
        /// <param name="Shape">Shape to intersect</param>
        /// <returns>Array of intersection points</returns>
        public override List<Vector2> IntersectPoints(I2DGeometry Shape)
        {
            Polygon2D polygon = ToPolygon2D();

            if (Shape is Path2D)
            {
                Shape = ((Path2D)Shape).ToPolygon2D();
            }

            // if shape is a polygon
            if (Shape is Polygon2D)
            {
                Polygon2D poly = (Polygon2D)Shape;
                return Polygon2D.IntersectPolygon2D(polygon, poly.Lines);
            }
            // if shape is a circle
            else if (Shape is Circle2D)
            {
                Circle2D circle = (Circle2D)Shape;
                return Polygon2D.IntersectCircle2D(polygon.Lines, circle.Center, circle.Radius);
            }
            // if shape is a line
            else if (Shape is Line2D)
            {
                Line2D line = (Line2D)Shape;
                return Polygon2D.IntersectLine2D(polygon.Lines,line.Points[0],line.Points[1]);
            }

            // if all else fails just return an empty array
            return new List<Vector2>();
        }

        /// <summary>
        /// Returns the points that intersect this path with a polygon.
        /// </summary>
        /// <param name="Polygon">Polygon</param>
        /// <param name="Lines">Lines</param>
        /// <returns>Intersection points</returns>
        public List<Vector2> IntersectPolygon2D(Polygon2D Polygon, Line2D[] Lines)
        {
            return Polygon2D.IntersectPolygon2D(Polygon, Lines);
        }

        /// <summary>
        /// Gets the intersection points between a path and a circle.
        /// </summary>
        /// <param name="Lines">Lines of the polygon</param>
        /// <param name="Center">The center of the circle</param>
        /// <param name="Radius">The radius of the circle</param>
        /// <returns>Intersection points</returns>
        public List<Vector2> IntersectCircle2D(Line2D[] Lines, Vector2 Center, float Radius)
        {
            return Polygon2D.IntersectCircle2D(Lines, Center, Radius);
        }

        /// <summary>
        /// Gets the intersection points between a path and a line.
        /// </summary>
        /// <param name="Lines">Lines of the polygon</param>
        /// <param name="Point1">The first point on the line</param>
        /// <param name="Point2">The second point on the line</param>
        /// <returns>Intersection points</returns>
        public List<Vector2> IntersectLine2D(Line2D[] Lines, Vector2 Point1, Vector2 Point2)
        {
            return Polygon2D.IntersectLine2D(Lines, Point1, Point2);
        }
        #endregion

        #region PointInGeometry

        /// <summary>
        /// Whether or not a point is inside of the path perimeter.
        /// </summary>
        /// <param name="Point">Point</param>
        /// <returns>True/False</returns>
        public override bool PointInGeometry(Vector2 Point)
        {
            return ToPolygon2D().PointInGeometry(Point);
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the Path into Dictionary.
        /// </summary>
        /// <returns>Serialized object</returns>
        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();

            List<MaxSerializedObject> points = new List<MaxSerializedObject>();
            for (int i = 0; i < Points.Length; i++)
            {
                points.Add(Vector2Serializer.Serialize(Points[i]));
            }

            Data.AddSerializedObjectCollection(SER_POINTS, points);
            Data.SetType(this);
            Data.AddString(SER_ENDSTATE, PathEndBehavior.ToString());

            return Data;
        }

        /// <summary>
        /// Deserializes the object from Dictionary data.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {

            if (Data.ContainsKey(SER_POINTS))
            {
                //get the points as an array somehow...
                List<MaxSerializedObject> points = Data.GetSerializedObjectCollection(SER_POINTS);
                Vector2[] pointData = new Vector2[points.Count];

                for (int i = 0; i < 2; i++)
                {
                    pointData[i] = Vector2Serializer.Deserialize(points[i]);
                }

                Points = pointData;

                List<Line2D> lines = new List<Line2D>();
                for (int i = 0; i < Points.Length; i++)
                {
                    // adds a line with the points Point[i] and Point[i+1] (but mod adjusted
                    // so it doesn't have an array out of bounds error and loops back to the first value
                    lines.Add(new Line2D(Points[i], Points[(i + 1) % Points.Length]));
                }
                Lines = lines.ToArray();
                Position = CurrentLine.Points[0];
            }

            // end behavior
            if (Data.ContainsKey(SER_ENDSTATE))
            {
                PathEndBehavior = (EndBehavior)Enum.Parse(typeof(EndBehavior), Data.GetString(SER_ENDSTATE));
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new Path2D();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
