﻿using System;
using System.Collections.Generic;
using max.ds;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;

namespace max.geometry.pathfinder
{
    internal class MapPathFinderNode : IComparable<MapPathFinderNode>
    {
        MapPathFinder Parent;

        /// <summary>The position of the test node.</summary>
        public Vector2 Position { get; protected set; }
        /// <summary>Neighbor nodes.</summary>
        public List<MapPathFinderNode> Neighbors { get; protected set; } = new List<MapPathFinderNode>();

        /// <summary>This is set to true if initialization fails (such as being inside a collision polygon).</summary>
        public bool Destroyed { get; protected set; }
        /// <summary>This is set to true if initialization completes.</summary>
        public bool Initialized { get; protected set; }
        /// <summary>The calculated shortest distance.</summary>
        public float ShortestDistance { get; set; }
        /// <summary>The neighbor node that is part of the shortest path.</summary>
        public MapPathFinderNode ShortestNode { get; set; }
        /// <summary>Used to hold the distance used for testing pathfinding.</summary>
        float TestDistance { get; set; }

        /// <summary>Initializes for the shortest distance.</summary>
        internal void InitShortestDistance()
        {
            ShortestDistance = float.MaxValue;
            ShortestNode = null;
        }
        /// <summary>Pathfinding calculation. </summary>
        internal void TestDijsktra()
        {
                //creates the priority queue
                PriorityQueue<MapPathFinderNode> TestDistances = new PriorityQueue<MapPathFinderNode>();
                MapPathFinderNode holder;
                float testDistance;

                //set all the test distances from this node to themselves
                //then enqueues them into the priority queue
                for (int i = 0; i < Neighbors.Count; i++)
                {
                    Neighbors[i].TestDistance = Vector2.Distance(Position, Neighbors[i].Position);
                    TestDistances.Enqueue(Neighbors[i]);
                }

                //keep dequeuing until the list is empty.
                while(TestDistances.Count() > 0)
                {
                    holder = TestDistances.Dequeue();

                    //do the actual DJ shit here.
                    //get distance
                    testDistance = ShortestDistance + holder.TestDistance;
                    if(testDistance < holder.ShortestDistance)
                    {
                        holder.ShortestDistance = testDistance;
                        holder.ShortestNode = this;
                    }

                }

                //When complete remove this node.
                Parent.NodeRemoveTestNodes(this);
        }

        public MapPathFinderNode(Vector2 position, List<Polygon2D> Collisions, MapPathFinder parent)
        {
            Parent = parent;
            //if the process is not already complete,
            //process node normally.
            Initialized = false;
            if (!Parent.IsComplete())
            { 
                Position = position;

                //test to make sure this node is not inside a collision.
                //Also that the node doesn't already exist.
                if (IsInCollision(Collisions))
                {
                    Destroyed = true;
                }
            }
            else
            {
                //if the path test is complete, this gets marked for destruction
                //and processing is ignored
                Destroyed = true;
            }
        }
        /// <summary>Tests whether or not this node is in a collision.</summary>
        private bool IsInCollision(List<Polygon2D> Collisions)
        {

            //Cycles through the collisions, if it is inside one of them
            //it marks it as true.
            //note it gets the polygon for each collision to test this.
            for (int i = 0; i < Collisions.Count; i++)
            {
                if (Collisions[i].PointInGeometry(Position))
                {
                    return true;
                }
            }

            //otherwise returns false
            return false;
        }
        /// <summary>Distance to a given node.</summary>
        /// <param name="n">Node to check</param>
        internal float DistanceToNode(MapPathFinderNode n)
        {
            return Vector2.Distance(Position, n.Position);
        }

        /// <summary>Distance to a given point.</summary>
        /// <param name="n">Point to check</param>
        internal float DistanceToPoint(Vector2 n)
            {
                return Vector2.Distance(Position, n);
            }
    
        /// <summary>Adds a node to the neighbor list.</summary>
        /// <param name="p">Node to add</param>
        internal void AddNode(MapPathFinderNode p)
        {
            if(p != null)
            {
                Neighbors.Add(p);
            }
        }

        /// <summary>Compares the test distance from this node to the passed node.</summary>
        /// <param name="p">Node to test</param>
        public int CompareTo(MapPathFinderNode other)
        {
            if (TestDistance < other.TestDistance) return -1;
            else if (TestDistance > other.TestDistance) return 1;
            else return 0;
        }
    }
}
