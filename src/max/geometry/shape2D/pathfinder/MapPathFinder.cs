﻿
using System;
using System.Collections.Generic;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;

namespace max.geometry.pathfinder
{
    public class MapPathFinder
    {
        /// <summary>
        /// Options for the pathfinder.
        /// </summary>
        public static PathOptions Options { get; protected set; } = new PathOptions();

        /// <summary>The list of nodes on the map.</summary>
        List<MapPathFinderNode> Nodes = new List<MapPathFinderNode>();

        /// <summary>The temporary holding list for calculating pathfinding.</summary>
        List<MapPathFinderNode> TestNodes = new List<MapPathFinderNode>();

        /// <summary>Counts how many nodes have been added.</summary>
        private int NodeCounter = 0;

        /// <summary>The starting position node.</summary>
        private MapPathFinderNode StartNode;
        /// <summary>The destination position node.</summary>
        private MapPathFinderNode EndNode;

        /// <summary>This signals that the pathfinding is completed.</summary>
        private bool Completed;

        private readonly float EPSILON = .01f;

        /// <summary>Whether or not diagonal paths should be tested for.</summary>
        public bool TestDiagonal { get; set; }

        public List<Polygon2D> Collisions { get; set; }

        /// <summary>
        /// Instantiates the path finder with an array of Polygon2D representing collisions.
        /// </summary>
        /// <param name="Collisions">Collision array</param>
        public MapPathFinder(List<Polygon2D> Collisions)
        {
            this.Collisions = Collisions;

        }

        /// <summary>
        /// Instantiates the path finder with an array of Polygon2D representing collisions,
        /// and a set of options.
        /// </summary>
        /// <param name="Collisions">Collision array</param>
        /// <param name="Options">Options</param>
        public MapPathFinder(List<Polygon2D> Collisions, PathOptions Options)
        {
            this.Collisions = Collisions;
            MapPathFinder.Options = Options;
        }

        /// <summary>Adds a node.</summary>
        /// <param name="Position">The position of the node to place</param>
        internal MapPathFinderNode AddNode(Vector2 Position)
        {
            MapPathFinderNode returner = new MapPathFinderNode(Position, Collisions,this);
            if (returner.Destroyed)
            {
                return null;
            }
            else
            {
                Nodes.Add(returner);
                NodeCounter++;
                return returner;
            }
        }

        /// <summary>Clear the node list.</summary>
        internal void ClearNodes()
        {
            Nodes.Clear();
            NodeCounter = 0;
            Completed = false;
        }
        /// <summary>Calculates the shortest path between two points.</summary>
        /// <param name="Point1">Starting point</param>
        /// <param name="Point2">Ending point</param>
        /// <returns>The path</returns>
        public Path2D GetPathBetweenPoints(Vector2 Point1, Vector2 Point2)
        {
            //initialize the shortest distances for all nodes.
            SetNodes();

            //gets a new path that has the new start and end node
            List<MapPathFinderNode> nodeList = new List<MapPathFinderNode>(Nodes);

            for (int i = 0; i < nodeList.Count; i++)
            {
                nodeList[i].InitShortestDistance();
            }

            EndNode = AddTestNode(nodeList, Point2);
            EndNode.ShortestNode = EndNode.Neighbors[0];

            StartNode = AddTestNode(nodeList, Point1);
            StartNode.ShortestDistance = 0;

            //add a variable to this and make it so that 
            //it is converted into vector3s
            Nodes = nodeList;
            return new Path2D(GetOptimizedPath(nodeList));
        }

        /// <summary>Adds a test node for pathfinding.</summary>
        private  MapPathFinderNode AddTestNode(List<MapPathFinderNode> list, Vector2 test)
        {
            MapPathFinderNode n = new MapPathFinderNode(test,Collisions,this);
            n.AddNode(GetClosestNode(n));
            list.Add(n);
            return n;
        }

        /// <summary>This generates the node list for the map. It will only produce nodes in places that are not inside collision polygons.</summary>
        internal void SetNodes()
        {
            //clears out the nodes just in case its not empty.
            ClearNodes();

            MapPathFinderNode[,] nodePos = new MapPathFinderNode
                                        [(int)Math.Ceiling((double)(Options.Width / Options.NodeSpacing)),
                                        (int)Math.Ceiling((double)(Options.Height / Options.NodeSpacing))];


            int newWidth = nodePos.GetLength(0)-1;
            int newLength = nodePos.GetLength(1)-1;

            //start at 0,0 and go out to max dist x and max dist y.
            for (int i = 0; i < Options.Width; i += Options.NodeSpacing)
            {
                for(int j = 0; j < Options.Height; j += Options.NodeSpacing)
                {
                    nodePos[Math.Min(i / Options.NodeSpacing,newWidth),Math.Min(j/Options.NodeSpacing,newLength)] = AddNode(new Vector2(i, j));
                }
            }

            //loop again to make connections
            for (int i = 0; i < newWidth; i++)
            {
                for (int j = 0; j < newLength; j++)
                {
                    if (nodePos[i, j] != null)
                    {
                        if (i > 0)
                        {
                            
                            nodePos[i, j].AddNode(nodePos[i - 1, j]);
                            if (TestDiagonal)
                            {
                                if (j > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j - 1]);
                                }
                                if (j < newLength - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j + 1]);
                                }
                            }
                        }
                        if (j > 0)
                        {
                            nodePos[i, j].AddNode(nodePos[i, j - 1]);
                            if (TestDiagonal)
                            {
                                if (i > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j - 1]);
                                }
                                if (i < newWidth - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j - 1]);
                                }
                            }
                        }
                        if (i < newWidth - 1)
                        {
                            nodePos[i, j].AddNode(nodePos[i + 1, j]);
                            if (TestDiagonal)
                            {
                                if (j > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j - 1]);
                                }
                                if (j < newLength - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j + 1]);
                                }
                            }
                        }
                        if (j < newLength - 1)
                        {
                            nodePos[i, j].AddNode(nodePos[i, j + 1]);
                            if (TestDiagonal)
                            {
                                if (i > 0)
                                {
                                    nodePos[i, j].AddNode(nodePos[i - 1, j + 1]);
                                }
                                if (i < newWidth - 1)
                                {
                                    nodePos[i, j].AddNode(nodePos[i + 1, j + 1]);
                                }
                            }
                        }
                    }
                }
            }


            for (int i = 0; i < Nodes.Count; i++)
            {
                //if a node still has no neighbors, find the closest node and give it that neighbor.
                if (Nodes[i].Neighbors.Count == 0)
                {
                    Nodes[i].AddNode(GetClosestNode(Nodes[i]));
                }
            }
        }


        /// <summary>Arranges the nodes to be processed for pathfinding.</summary>
        private  List<Vector2> GetOptimizedPath(List<MapPathFinderNode> n)
        {
            List<Vector2> list = new List<Vector2>();
            TestNodes = new List<MapPathFinderNode>(n);
            List<MapPathFinderNode> EvalNodes = new List<MapPathFinderNode>();
            MapPathFinderNode processMe;

            EvalNodes.Add(StartNode);

            int counter = 0;
            //while there are still nodes to evaluate.
            while (EvalNodes.Count > 0)
            {
                //evaluate the top node
                processMe = EvalNodes[0];
                EvalNodes.RemoveAt(0);
                if(processMe != null)
                {
                    if (NodeInTestNodes(processMe))
                    {
                        processMe.TestDijsktra();
                        for (int i = 0; i < processMe.Neighbors.Count; i++)
                        {
                            if (NodeInTestNodes(processMe.Neighbors[i]) && EvalNodes.IndexOf(processMe.Neighbors[i]) == -1)
                            {
                                EvalNodes.Add(processMe.Neighbors[i]);
                            }
                        }
                    }
                }

                counter++;
            }

            //set this to the destination node
            processMe = EndNode;
            //create the list representing the shortest path
            while(processMe != null)
            {
                list.Add(processMe.Position);
                processMe = processMe.ShortestNode;
            }
            Nodes = n;
            list.Reverse();
            return list;
        }

        /// <summary>Whether a test node is inside the list of test nodes.</summary>
        internal  bool NodeInTestNodes(MapPathFinderNode n)
        {
            if (TestNodes.Find(x => x == n) != null)
            {
                return true;
            }

            return false;
        }
        /// <summary>Removes a test node from the list of test nodes.</summary>
        internal void NodeRemoveTestNodes(MapPathFinderNode n)
        {
            TestNodes.Remove(n);
        }

        /// <summary>Determines whether or not pathfinding has completed.</summary>
        internal bool IsComplete()
        {
            return Completed || NodeCounter > Options.MaxNodes;
        }

        /// <summary>Checks whether or not a test node already exists in the test node list.</summary>
        /// <param name="n">The test node to check</param>
        internal bool NodeAlreadyExists(MapPathFinderNode n)
        {
            if (Nodes.Find((MapPathFinderNode obj) => obj.DistanceToNode(n) < Options.NodeSpacing) == null){
                return false;
            }
            return true;
        }
        /// <summary>Gets the closest node to a certain node.</summary>
        /// <param name="n">The node to test the closest node to</param>
        internal MapPathFinderNode GetClosestNode(MapPathFinderNode n)
        {
            float distance = float.MaxValue;
            float oldDistance;
            MapPathFinderNode returner = null;
            for (int i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i] != n)
                {
                    oldDistance = distance;
                    distance = Math.Min(distance, Nodes[i].DistanceToNode(n));
                    if (Math.Abs(oldDistance - distance) > EPSILON)
                    {
                        returner = Nodes[i];
                    }
                }
            }

            return returner;
        }
    }

    /// <summary>
    /// This class contains all the path options.
    /// </summary>
    public sealed class PathOptions
    {
        /// <summary>
        /// <para>The maximum number of nodes that the pathfinding engine will use at any time.</para>
        /// </summary>
        /// <value>Quantity</value>
        public int MaxNodes { get; set; } = 50000;
        /// <summary>
        /// <para>The distance between nodes for pathfinding.</para>
        /// <para>The higher this value, the less nodes are created and less accurate the pathfinding.</para>
        /// </summary>
        /// <value>Distance</value>
        public int NodeSpacing { get; set; } = 15;

        /// <summary>
        /// <para>The map width in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Width { get; set; } = 800;
        /// <summary>
        /// <para>The map height in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Height { get; set; } = 600;
    }
}
