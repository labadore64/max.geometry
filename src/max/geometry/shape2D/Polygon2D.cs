﻿using max.geometry.helper;
using max.serialize;
using max.serialize.type;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace max.geometry.shape2D
{
    /// <summary>
    /// This class represents a polygon on a 2D plane.
    /// </summary>
    public class Polygon2D : Geometry2D
    {
        #region Constants
        // strings used for serialization key names
        const string SER_POINTS = "points";

        #endregion

        #region Private Variables

        private readonly List<Vector2> _points;
        private readonly List<Vector2> _edges;

        #endregion

        #region Properties

        /// <summary>
        /// Number of points in the polygon.
        /// </summary>
        /// <value>Number of points</value>
        public int Count { get { return _points.Count; } }

        /// <summary>
        /// The list of points.
        /// </summary>
        /// <value>Points</value>
        public override Vector2[] Points
        {
            get {
                return _points.ToArray();
            }
            protected set
            {
                _points.Clear();
                for (int i = 0; i < value.Length; i++)
                {
                    if (_points.Contains(value[i]))
                    {
                        throw new ArgumentException("This point already exists! (" + value[i].X + ", " + value[i].Y + ")");
                    }
                    else
                    {
                        _points.Add(value[i]);
                    }
                }
                SetCenter();
                RebuildEdges();
                List<Line2D> lines = new List<Line2D>();
                for (int i = 0; i < _points.Count; i++)
                {
                    // adds a line with the points Point[i] and Point[i+1] (but mod adjusted
                    // so it doesn't have an array out of bounds error and loops back to the first value
                    lines.Add(new Line2D(Points[i], Points[(i + 1) % _points.Count]));
                }

                _lines = lines.ToArray();
            }
        }

        /// <summary>
        /// The list of edges.
        /// </summary>
        /// <value>Edges</value>
        public Vector2[] Edges
        {
            get => _edges.ToArray();
        }

        /// <summary>
        /// The lines that make up the polygon.
        /// </summary>
        /// <value>Lines</value>
        public Line2D[] Lines
        {
            get
            {
                return _lines;
            }
        }

        Line2D[] _lines;

        /// <summary>
        /// The center point of the polygon.
        /// </summary>
        /// <value>Center</value>
        public override Vector2 Center
        {
            get
            {
                return _center;
            }
        }

        Vector2 _center;

        #endregion

        #region Constructors

        /// <summary>
        /// Instantiates an empty polygon.
        /// </summary>
        public Polygon2D()
        {
            _points = new List<Vector2>();
            _edges = new List<Vector2>();
        }

        /// <summary>
        /// Instantiates a polygon from serialized data.
        /// </summary>
        /// <param name="Data">Data</param>
        public Polygon2D(MaxSerializedObject Data)
        {
            _points = new List<Vector2>();
            _edges = new List<Vector2>();
            Deserialize(Data);
        }

        /// <summary>
        /// Instantiates a polygon from serialized data.
        /// </summary>
        /// <param name="Data">Data</param>
        public Polygon2D(object Data)
        {
            _points = new List<Vector2>();
            _edges = new List<Vector2>();
            Deserialize((MaxSerializedObject)Data);
        }

        /// <summary>
        /// Instantiates a polygon from an array of Vector2 points.
        /// </summary>
        /// <param name="Points">Points</param>
        public Polygon2D(Vector2[] Points)
        {
            _points = new List<Vector2>();
            _edges = new List<Vector2>();
            this.Points = Points;
        }

        /// <summary>
        /// Instantiates a polygon from another Polygon2D
        /// </summary>
        /// <param name="Polygon">Polygon</param>
        public Polygon2D(Polygon2D Polygon)
        {
            _points = new List<Vector2>();
            _edges = new List<Vector2>();
            Points = Polygon.Points;
        }
        #endregion

        #region AddPoint

        // this method is a non-static only method

        /// <summary>
        /// Adds a point to the polygon.
        /// </summary>
        /// <param name="Point">The point to add</param>
        public void AddPoint(Vector2 Point)
        {
            if (_points.Contains(Point))
            {
                throw new ArgumentException("This point already exists", nameof(Point));
            }
            else
            {
                _points.Add(Point);
            }
            RebuildEdges();
        }

        /// <summary>
        /// Adds a point to the polygon, using float values.
        /// </summary>
        /// <param name="X">The x coordinate of the point to add</param>
        /// <param name="Y">The y coordinate of the point to add</param>
        public void AddPoint(float X, float Y)
        {
            Vector2 point = new Vector2(X, Y);
            AddPoint(point);
        }

        #endregion

        #region RebuildEdges

        // this method is a non-static only method

        /// <summary>
        /// Rebuilds the edges of the polygon.
        /// </summary>
        private void RebuildEdges()
        {
            _edges.Clear();
            for (int i = 0; i < _points.Count - 1; i++)
            {
                _edges.Add(_points[i + 1] - _points[i]);
            }
            _edges.Add(_points[_points.Count - 1] - _points[0]);
        }

        #endregion

        #region GetCenter

        /// <summary>
        /// Gets the center of the polygon defined by the shape.
        /// </summary>
        /// <param name="Shape">Points of shape</param>
        /// <returns></returns>
        public static Vector2 GetCenter(IList<Vector2> Shape)
        {
            float totalX = 0;
            float totalY = 0;
            for (int i = 0; i < Shape.Count; i++)
            {
                totalX += Shape[i].X;
                totalY += Shape[i].Y;
            }

            return new Vector2(totalX / Shape.Count, totalY / Shape.Count);
        }

        /// <summary>
        /// Gets the center of the polygon defined by the shape.
        /// </summary>
        /// <param name="Shape">Points of shape</param>
        /// <returns></returns>
        public static Vector2 GetCenter(Vector2[] Shape)
        {
            float totalX = 0;
            float totalY = 0;
            for (int i = 0; i < Shape.Length; i++)
            {
                totalX += Shape[i].X;
                totalY += Shape[i].Y;
            }

            return new Vector2(totalX / Shape.Length, totalY / Shape.Length);
        }

        private void SetCenter()
        {
            _center = GetCenter(_points);
        }

        #endregion

        #region PointInGeometry

        /// <summary>
        /// Returns whether or not a point is in the perimeter of the polygon.
        /// </summary>
        /// <param name="Point">The point to test</param>
        /// <returns>True/False</returns>
        public override bool PointInGeometry(Vector2 Point)
        {
            return PointInGeometry(_points, Point);
        }

        /// <summary>
        /// Returns whether or not a point is in the perimeter of the shape.
        /// </summary>
        /// <param name="Shape">The shape</param>
        /// <param name="Point">The point to test</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(Vector2[] Shape, Vector2 Point)
        {
            bool result = false;
            int j = Shape.Length - 1;

            for (int i = 0; i < Shape.Length; i++)
            {
                if (Shape[i].Y < Point.Y && Shape[j].Y >= Point.Y || Shape[j].Y < Point.Y && Shape[i].Y >= Point.Y)
                {
                    if (Shape[i].X + (Point.Y - Shape[i].Y) / (Shape[j].Y - Shape[i].Y) * (Shape[j].X - Shape[i].X) < Point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

        /// <summary>
        /// Returns whether or not a point is in the perimeter of the shape.
        /// </summary>
        /// <param name="Shape">The shape</param>
        /// <param name="Point">The point to test</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(IList<Vector2> Shape, Vector2 Point)
        {
            bool result = false;
            int j = Shape.Count - 1;

            for (int i = 0; i < Shape.Count; i++)
            {
                if (Shape[i].Y < Point.Y && Shape[j].Y >= Point.Y || Shape[j].Y < Point.Y && Shape[i].Y >= Point.Y)
                {
                    if (Shape[i].X + (Point.Y - Shape[i].Y) / (Shape[j].Y - Shape[i].Y) * (Shape[j].X - Shape[i].X) < Point.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

        #endregion

        #region LineIntersectDistance

        /// <summary>
        /// Returns the distance of the closest intersection
        /// with a line relative to Point1.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Distance</returns>
        public float LineIntersectDistance(Vector2 Point1, Vector2 Point2)
        {
            return LineIntersectDistance(_points, Point1, Point2);
        }

        /// <summary>
        /// Returns the distance of the closest intersection
        /// with a line relative to Line.StartPoint.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>Distance</returns>
        public float LineIntersectDistance(Line2D Line)
        {
            return LineIntersectDistance(Line.Points[0], Line.Points[1]);
        }

        /// <summary>
        /// Returns the distance of the closest intersection of a polygon,
        /// defined as am IList, with a line relative to Point1.
        /// </summary>
        /// <param name="ShapePoints">Polygon2D</param>
        /// <param name="Point1">The first point and the distance point to test</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Distance</returns>
        public static float LineIntersectDistance(IList<Vector2> ShapePoints, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> test;
            float returner = 1000000;

            for (int i = 0; i < ShapePoints.Count; i++)
            {

                test = Line2D.IntersectLine2D(Point1, Point2,
                    ShapePoints[i],
                    ShapePoints[(i + 1) % ShapePoints.Count]);

                if (test.Count > 0)
                {
                    returner = Math.Min(returner, Vector2.Distance(test[0], Point1));
                }

            }

            return returner;
        }

        /// <summary>
        /// Returns the distance of the closest intersection of a polygon,
        /// defined as a Vector2 array, with a line relative to Point1.
        /// </summary>
        /// <param name="Points">Polygon2D</param>
        /// <param name="Point1">The first point and the distance point to test</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Distance</returns>
        public static float LineIntersectDistance(Vector2[] Points, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> test;
            float returner = float.MaxValue;

            for (int i = 0; i < Points.Length; i++)
            {

                test = Line2D.IntersectLine2D(Point1, Point2,
                    Points[i],
                    Points[(i + 1) % Points.Length]);

                if (test.Count > 0)
                {
                    returner = Math.Min(returner, Vector2.Distance(test[0], Point1));
                }

            }

            return returner;
        }

        #endregion

        #region LineIntersection

        /// <summary>
        /// Returns the closest intersection point with a line,
        /// relative to Point1.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Intersection point</returns>
        public Vector2 LineIntersection(Vector2 Point1, Vector2 Point2)
        {
            return LineIntersection(_points, Point1, Point2);
        }

        /// <summary>
        /// Returns the closest intersection point with a line,
        /// relative to Point1.
        /// </summary>
        /// <param name="Line">Line</param>
        /// <returns>Intersection point</returns>
        public Vector2 LineIntersection(Line2D Line)
        {
            return LineIntersection(Line.Points[0], Line.Points[1]);
        }

        /// <summary>
        /// Returns the closest intersection point with a line,
        /// relative to Point1.
        /// </summary>
        /// <param name="Shape">Shape as points</param>
        /// <param name="Point1">Start point</param>
        /// <param name="Point2">End point</param>
        /// <returns>Intersection point</returns>
        public static Vector2 LineIntersection(Vector2[] Shape, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> test;
            Vector2 ClosestPoint = Vector2Helper.DUMMY;
            float testDistance = float.MaxValue;
            for (int i = 0; i < Shape.Length; i++)
            {
                test = Line2D.IntersectLine2D(Point1, Point2,
                    new Vector2(Shape[i].X, Shape[i].Y),
                    new Vector2(Shape[(i + 1) % Shape.Length].X, Shape[(i + 1) % Shape.Length].Y)
                );
                if (test.Count > 0)
                {
                    testDistance = Math.Min(testDistance, Vector2.Distance(test[0], Point1));
                    ClosestPoint = test[0];
                }
            }

            return ClosestPoint;
        }

        /// <summary>
        /// Returns the closest intersection point with a line,
        /// relative to Point1.
        /// </summary>
        /// <param name="Shape">Shape as points</param>
        /// <param name="Point1">Start point</param>
        /// <param name="Point2">End point</param>
        /// <returns>Intersection point</returns>
        public static Vector2 LineIntersection(IList<Vector2> Shape, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> test;
            Vector2 ClosestPoint = Vector2Helper.DUMMY;
            float testDistance = float.MaxValue;
            for (int i = 0; i < Shape.Count; i++)
            {
                test = Line2D.IntersectLine2D(Point1, Point2,
                    new Vector2(Shape[i].X, Shape[i].Y),
                    new Vector2(Shape[(i + 1) % Shape.Count].X, Shape[(i + 1) % Shape.Count].Y)
                );
                if (test.Count > 0)
                {
                    testDistance = Math.Min(testDistance, Vector2.Distance(test[0], Point1));
                    //collision detected
                    ClosestPoint = test[0];
                }
            }

            return ClosestPoint;
        }

        #endregion

        #region PointsEqual

        /// <summary>
        /// Tests whether or not an array of Vector2 are the same
        /// as the polygon's points.
        /// </summary>
        /// <param name="Points">The points to test</param>
        /// <returns>True/False</returns>
        public bool PointsEqual(Vector2[] Points)
        {
            return PointsEqual(_points, Points);
        }

        /// <summary>
        /// Tests whether or not an array of Vector2 are the same
        /// as a polygon's points.
        /// </summary>
        /// <param name="Shape">The polygon</param>
        /// <param name="Points">The points to test</param>
        /// <returns>True/False</returns>
        public static bool PointsEqual(IList<Vector2> Shape, Vector2[] Points)
        {
            if (Points.Length != Shape.Count)
            {
                return false;
            }

            for (int i = 0; i < Points.Length; i++)
            {

                if (Points[i] != Shape[i])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Tests whether or not an array of Vector2 are the same
        /// as a polygon's points.
        /// </summary>
        /// <param name="Shape">The polygon</param>
        /// <param name="Points">The points to test</param>
        /// <returns>True/False</returns>
        public static bool PointsEqual(Vector2[] Shape, Vector2[] Points)
        {
            if (Points.Length != Shape.Length)
            {
                return false;
            }

            for (int i = 0; i < Points.Length; i++)
            {
                for (int j = 0; j < Shape.Length; j++)
                {
                    //if the points aren't identitcal,
                    //return false
                    if (Points[i] != Shape[j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        #endregion

        #region ToPath2D

        // this method is redundant if static so its only non-static

        /// <summary>
        /// Returns a Path2D made from the points of this polygon.
        /// </summary>
        /// <returns>Path2D</returns>
        public Path2D ToPath2D()
        {
            return new Path2D(_points);
        }
        #endregion

        #region Copy
        /// <summary>
        /// Copies this Polygon as an object.
        /// </summary>
        /// <returns>Copied shape</returns>
        public override I2DGeometry Copy()
        {
            return new Polygon2D(this);
        }

        /// <summary>
        /// Returns a copy of the source polygon.
        /// </summary>
        /// <param name="Source">Source polygon</param>
        /// <returns>Copied polygon</returns>
        public static Polygon2D Copy(Polygon2D Source)
        {
            return (Polygon2D)Source.Copy();
        }
        #endregion

        #region IntersectPoints

        /// <summary>
        /// Returns the points that intersect this shape with another.
        /// </summary>
        /// <param name="Shape">Shape to intersect</param>
        /// <returns>Intersection points</returns>
        public override List<Vector2> IntersectPoints(I2DGeometry Shape)
        {
            if (Shape is Path2D)
            {
                Shape = ((Path2D)Shape).ToPolygon2D();
            }

            // if shape is a polygon
            if (Shape is Polygon2D)
            {
                Polygon2D Polygon = (Polygon2D)Shape;
                Line2D[] Lines = Polygon.Lines;
                return IntersectPolygon2D(this, Lines);
            }
            // if shape is a circle
            else if (Shape is Circle2D)
            {
                Circle2D circle = (Circle2D)Shape;
                return Circle2D.IntersectPolygon2D(circle.Center, circle.Radius, Lines);
            }
            // if shape is a line
            else if (Shape is Line2D)
            {
                return Line2D.IntersectPolygon2D(Shape.Points[0], Shape.Points[1], Lines);
            }

            // if all else fails just return an empty array
            return new List<Vector2>();
        }

        /// <summary>
        /// Intersects a polygon with a set of lines.
        /// </summary>
        /// <param name="Polygon">Polygon</param>
        /// <param name="Lines">Lines</param>
        /// <returns>Intersections</returns>
        public static List<Vector2> IntersectPolygon2D(Polygon2D Polygon, Line2D[] Lines)
        {
            List<Vector2> intersections = new List<Vector2>();

            for(int i = 0; i < Lines.Length; i++)
            {
                intersections.AddRange(
                    Line2D.IntersectPolygon2D(
                        Lines[i].Points[0], 
                        Lines[i].Points[1], 
                        Polygon.Lines));
            }

            return intersections;
        }

        /// <summary>
        /// Gets the intersection points between a polygon and a line.
        /// </summary>
        /// <param name="Lines">Lines of the polygon</param>
        /// <param name="Point1">The first point on the line</param>
        /// <param name="Point2">The second point on the line</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectLine2D(Line2D[] Lines, Vector2 Point1, Vector2 Point2)
        {
            return Line2D.IntersectPolygon2D(Point1, Point2, Lines);
        }

        /// <summary>
        /// Gets the intersection points between a polygon and a circle.
        /// </summary>
        /// <param name="Lines">Lines of the polygon</param>
        /// <param name="Center">The center of the circle</param>
        /// <param name="Radius">The radius of the circle</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectCircle2D(Line2D[] Lines, Vector2 Center, float Radius)
        {
            return Circle2D.IntersectPolygon2D(Center, Radius, Lines);
        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the Polygon2D into a Dictionary.
        /// </summary>
        /// <returns>Serialized object</returns>
        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject data = MaxSerializedObject.Create();

            List<MaxSerializedObject> points = new List<MaxSerializedObject>();
            for (int i = 0; i < Points.Length; i++)
            {
                points.Add(Vector2Serializer.Serialize(Points[i]));
            }
            data.SetType(this);
            data.AddVector2Collection(SER_POINTS, Points);
            return data;
        }

        /// <summary>
        /// Deserializes a Dictionary into a Polygon2D.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            List<Vector2> points = Data.GetVector2List(SER_POINTS);

            Points = points.ToArray();
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new Polygon2D();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
