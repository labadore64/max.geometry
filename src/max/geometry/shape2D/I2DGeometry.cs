﻿using max.serialize;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace max.geometry.shape2D
{
    /// <summary>
    /// This interface is used for 2D geometry.
    /// </summary>
    public interface I2DGeometry : IMaxSerializable
    {
        /// <summary>
        /// The center of the geometry.
        /// </summary>
        Vector2 Center { get; }

        /// <summary>
        /// Gets the points of the geometry.
        /// </summary>
        Vector2[] Points { get; }

        /// <summary>
        /// Returns a copy of the current geometry.
        /// </summary>
        /// <returns>The copied shape.</returns>
        I2DGeometry Copy();

        /// <summary>
        /// Returns how many points intersect with this shape.
        /// </summary>
        /// <param name="Shape">Shape to intersect with</param>
        /// <returns>Intersection points</returns>
        List<Vector2> IntersectPoints(I2DGeometry Shape);

        /// <summary>
        /// Returns whether or not a point is inside this shape's geometry.
        /// </summary>
        /// <param name="Point">The point to test.</param>
        /// <returns>True/False</returns>
        bool PointInGeometry(Vector2 Point);
    }
}
