﻿using max.serialize;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;

namespace max.geometry.shape2D
{
    /// <summary>
    /// This class represents a circle on a 2D plane.
    /// </summary>
    public class Circle2D : Geometry2D
    {
        #region Constants

        // strings used for serialization key names
        const string SER_POS = "position";
        const string SER_RADIUS = "radius";

        #endregion

        #region Properties

        /// <summary>
        /// The radius of the circle.
        /// </summary>
        /// <value>Radius</value>
        public float Radius { get; set; }

        /// <summary>
        /// Returns the circumfrence of the circle.
        /// </summary>
        /// <value>Circumference</value>
        public float Circumference { get { return (float)(Radius * 2 * Math.PI); } }

        /// <summary>
        /// Returns the area of the circle.
        /// </summary>
        /// <value>Area</value>
        public float Area { get { return (float)(Radius * Radius * Math.PI); } }

        /// <summary>
        /// Returns the center as the points array.
        /// </summary>
        /// <value>Center</value>
        public override Vector2[] Points
        {
            get
            {
                return new Vector2[] { Center };
            }
            protected set
            {
                if(value.Length > 0)
                {
                    Center = value[0];
                }
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a circle at (0,0) with a radius of 1.
        /// </summary>
        public Circle2D()
        {
            Center = Vector2.Zero;
            Radius = 1;
        }

        /// <summary>
        /// Creates a circle at the position with a radius of 1.
        /// </summary>
        /// <param name="Center">Position of center</param>
        public Circle2D(Vector2 Center)
        {
            this.Center = Center;
            Radius = 1;
        }

        /// <summary>
        /// Creates a circle.
        /// </summary>
        /// <param name="Center">Position of center</param>
        /// <param name="Radius">Radius</param>
        public Circle2D(Vector2 Center, float Radius)
        {
            this.Center = Center;
            this.Radius = Radius;
        }

        /// <summary>
        /// Creates a circle.
        /// </summary>
        /// <param name="Circle">Circle to copy from</param>
        public Circle2D(Circle2D Circle)
        {
            Center = Circle.Center;
            Radius = Circle.Radius;
        }


        #endregion

        #region LineIntersectionDistance

        /// <summary>
        /// Returns the distance from the first point to the first point of intersection against this circle.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Distance</returns>
        public float LineIntersectionDistance(Vector2 Point1, Vector2 Point2)
        {
            return LineIntersectionDistance(Center, Radius, Point1, Point2);
        }

        /// <summary>
        /// Returns the distance from Line.Points[0] to the first point of intersection against this circle.
        /// </summary>
        /// <param name="Line">The line</param>
        /// <returns>Distance</returns>
        public float LineIntersectionDistance(Line2D Line)
        {
            return LineIntersectionDistance(Center, Radius, Line.Points[0], Line.Points[1]);
        }

        /// <summary>
        /// Returns the distance from the first point to the first point of intersection against this circle.
        /// </summary>
        /// <param name="Radius">The radius of the circle</param>
        /// <param name="Center">The center of the circle</param>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>Distance</returns>
        public static float LineIntersectionDistance(Vector2 Center, float Radius, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> points = LineIntersectClosestPoint(Center, Radius, Point1, Point2);
            if (points.Count == 0)
            {
                return -1;
            }
            else
            {
                return Vector2.Distance(Point1, points[0]);
            }
        }

        #endregion

        #region LineIntersectClosestPoint

        /// <summary>
        /// Returns the closest intersection point of a line against this circle, relative to Point1.
        /// </summary>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>A 1 element array with the point if its found. Otherwise an empty array.</returns>
        public List<Vector2> LineIntersectClosestPoint(Vector2 Point1, Vector2 Point2)
        {
            return LineIntersectClosestPoint(Center,Radius, Point1, Point2);
        }

        /// <summary>
        /// Returns the closest intersection point of a line against this circle, relative to Line.StartPoint.
        /// </summary>
        /// <param name="Line">The line</param>
        /// <returns>A 1 element array with the point if its found. Otherwise an empty array.</returns>
        public List<Vector2> LineIntersectClosestPoint(Line2D Line)
        {
            return LineIntersectClosestPoint(Line.Points[0], Line.Points[1]);
        }

        /// <summary>
        /// Returns the closest intersection point of a line against this circle, relative to Point1.
        /// </summary>
        /// <param name="Radius">The radius of the circle</param>
        /// <param name="Center">The center of the circle</param>
        /// <param name="Point1">The first point</param>
        /// <param name="Point2">The second point</param>
        /// <returns>A 1 element array with the point if its found. Otherwise an empty array.</returns>
        public static List<Vector2> LineIntersectClosestPoint(Vector2 Center, float Radius, Vector2 Point1, Vector2 Point2)
        {
            List<Vector2> points = IntersectLine2D(Center, Radius, Point1, Point2);

            if (points.Count < 2)
            {
                return points;
            }

            Vector2 closestPoint = Vector2.Zero;
            List<Vector2> returner = new List<Vector2>();
            float dist = float.MaxValue;
            float testdist;

            for (int i = 0; i < points.Count; i++)
            {
                testdist = Vector2.Distance(Point1, points[i]);
                if (testdist < dist)
                {
                    dist = testdist;
                    closestPoint = points[i];
                }
            }

            returner.Add(closestPoint);

            return returner;
        }

        #endregion

        #region PointInGeometry

        /// <summary>
        /// Whether or not the point lies in the circle.
        /// </summary>
        /// <param name="Point">Point</param>
        /// <returns>True/False</returns>
        public override bool PointInGeometry(Vector2 Point)
        {
            return PointInGeometry(Center, Radius, Point);
        }

        /// <summary>
        /// Whether or not the point lies in a circle.
        /// </summary>
        /// <param name="Point">Point</param>
        /// <param name="Center">Center</param>
        /// <param name="Radius">Radius</param>
        /// <returns>True/False</returns>
        public static bool PointInGeometry(Vector2 Center, float Radius, Vector2 Point)
        {
            // Compare radius of circle with 
            // distance of its center from 
            // given point 
            if ((Point.X - Center.X) * (Point.X - Center.X) +
                (Point.Y - Center.Y) * (Point.Y - Center.Y) <= Radius * Radius)
                return true;
            else
                return false;
        }

        #endregion

        #region PointOnCircle

        /// <summary>
        /// Returns the point on a circle based on the angle in radians.
        /// </summary>
        /// <param name="Angle">Angle in radians</param>
        /// <returns>Point</returns>
        public Vector2 PointOnCircle(float Angle)
        {
            return PointOnCircle(Center, Radius, Angle);
        }

        /// <summary>
        /// Returns the point on the circle based on the angle in radians.
        /// </summary>
        /// <param name="Circle">Circle</param>
        /// <param name="Angle">Angle in radians</param>
        /// <returns>Point</returns>
        public static Vector2 PointOnCircle(Circle2D Circle, float Angle)
        {
            return Circle.PointOnCircle(Angle);
        }

        /// <summary>
        /// Returns the point on a circle based on the angle in radians.
        /// </summary>
        /// <param name="Radius">Radius of the circle</param>
        /// <param name="Center">Position of the center of the circle</param>
        /// <param name="Angle">Angle in radians</param>
        /// <returns>Point</returns>
        public static Vector2 PointOnCircle(Vector2 Center, float Radius, float Angle)
        {
            return new Vector2(
                (float)(Math.Cos(Angle) * Radius + Center.X),
                (float)(Math.Sin(Angle) * Radius + Center.Y)
            );
        }

        #endregion

        #region Copy
        /// <summary>
        /// Copies this Circle as an object.
        /// </summary>
        /// <returns>Copied shape</returns>
        public override I2DGeometry Copy()
        {
            return new Circle2D(this);
        }

        /// <summary>
        /// Returns a copy of the source circle.
        /// </summary>
        /// <param name="Source">Source circle</param>
        /// <returns>Copied circle</returns>
        public static Circle2D Copy(Circle2D Source)
        {
            return (Circle2D)Source.Copy();
        }
        #endregion

        #region IntersectPoints

        /// <summary>
        /// Returns the points that intersect this shape with another.
        /// </summary>
        /// <param name="Shape">Shape to intersect</param>
        /// <returns>Array of intersection points</returns>
        public override List<Vector2> IntersectPoints(I2DGeometry Shape)
        {
            if (Shape is Path2D)
            {
                Shape = ((Path2D)Shape).ToPolygon2D();
            }

            // if shape is a polygon
            if(Shape is Polygon2D)
            {
                Polygon2D Polygon = (Polygon2D)Shape;
                Line2D[] Lines = Polygon.Lines;
                return IntersectPolygon2D(Center, Radius, Lines);
            }
            // if shape is a circle
            else if (Shape is Circle2D)
            {
                Circle2D circle = (Circle2D)Shape;
                return IntersectCircle2D(Center, Radius, circle.Center, circle.Radius);
            } 
            // if shape is a line
            else if (Shape is Line2D)
            {
                return IntersectLine2D(Center, Radius, Shape.Points[0], Shape.Points[1]);
            }

            // if all else fails just return an empty list
            return new List<Vector2>();
        }

        /// <summary>
        /// Returns the points a circle intersects a Polygon.
        /// </summary>
        /// <param name="Center">Center of the circle</param>
        /// <param name="Radius">Radius of the circle</param>
        /// <param name="Lines">Lines of polygon intersecting the circle</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectPolygon2D(Vector2 Center, float Radius, Line2D[] Lines)
        {
            // get all the intersections of each line

            List<List<Vector2>> LineCollisions = new List<List<Vector2>>();


            for (int i = 0; i < Lines.Length; i++)
            {
                LineCollisions.Add(IntersectLine2D(Center, Radius, Lines[0].Points[0], Lines[0].Points[1]));
            }

            // put all the results together in one array

            List<Vector2> Results = new List<Vector2>();

            for (int i = 0; i < LineCollisions.Count; i++)
            {
                for (int j = 0; j < LineCollisions[i].Count; j++)
                {
                    Results.Add(LineCollisions[i][j]);
                }
            }

            return Results;
        }

        /// <summary>
        /// Returns the points a circle intersects a line.
        /// </summary>
        /// <param name="Center">Center of the circle</param>
        /// <param name="Radius">Radius of the circle</param>
        /// <param name="Point1">The starting point of the line</param>
        /// <param name="Point2">The end point of the line</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectLine2D(Vector2 Center, float Radius, Vector2 Point1, Vector2 Point2)
        {
            float dx, dy, A, B, C, det, t;

            dx = Point2.X - Point1.X;
            dy = Point2.Y - Point1.Y;

            A = dx * dx + dy * dy;
            B = 2 * (dx * (Point1.X - Center.X) + dy * (Point1.Y - Center.Y));
            C = (Point1.X - Center.X) * (Point1.X - Center.X) + (Point1.Y - Center.Y) * (Point1.Y - Center.Y) - Radius * Radius;

            det = B * B - 4 * A * C;
            List<Vector2> results = new List<Vector2>();
            if (!((A <= 0.0000001) || (det < 0)))
            {
                if (det == 0)
                {
                    // One solution.
                    t = -B / (2 * A);
                    results.Add(new Vector2(Point1.X + t * dx, Point1.Y + t * dy));
                }
                else
                {
                    // Two solutions.
                    t = (float)((-B + Math.Sqrt(det)) / (2 * A));
                    results.Add(new Vector2(Point1.X + t * dx, Point1.Y + t * dy));
                    t = (float)((-B - Math.Sqrt(det)) / (2 * A));
                    results.Add(new Vector2(Point1.X + t * dx, Point1.Y + t * dy));
                }
            }
            //make sure each point detected is actually on the line
            for (int i = 0; i < results.Count; i++)
            {
                if (!Line2D.PointInGeometry(Point1, Point2, results[i]))
                {
                    results.RemoveAt(i);
                    i--;
                }
            }

            return results;
        }

        /// <summary>
        /// Returns the points a circle intersects another circle
        /// </summary>
        /// <param name="Center1">Center of the first circle</param>
        /// <param name="Radius1">Radius of the first circle</param>
        /// <param name="Center2">Center of the second circle</param>
        /// <param name="Radius2">Radius of the second circle</param>
        /// <returns>Intersection points</returns>
        public static List<Vector2> IntersectCircle2D(Vector2 Center1, float Radius1, Vector2 Center2, float Radius2)
        {
            List<Vector2> list = new List<Vector2>();

            // Find the distance between the centers.
            float dx = Center1.X - Center2.X;
            float dy = Center1.Y - Center2.X;
            double dist = Math.Sqrt(dx * dx + dy * dy);

            // See how many solutions there are.
            if (dist > Radius1 + Radius2)
            {
                // No solutions, the circles are too far apart.
                return list;
            }
            else if (dist < Math.Abs(Radius1 - Radius2))
            {
                // No solutions, one circle contains the other.
                return list;
            }
            else if ((dist == 0) && (Radius1 == Radius2))
            {
                // No solutions, the circles coincide.
                return list;
            }
            else
            {
                // Find a and h.
                double a = (Radius1 * Radius1 -
                    Radius2 * Radius2 + dist * dist) / (2 * dist);
                double h = Math.Sqrt(Radius1 * Radius1 - a * a);

                // Find P2.
                double cx2 = Center1.X + a * (Center2.X - Center1.X) / dist;
                double cy2 = Center1.Y + a * (Center2.X - Center1.Y) / dist;

                // Get the points P3.
                Vector2[] returner = new Vector2[] {
                    new Vector2(
                    (float)(cx2 + h * (Center2.X - Center1.Y) / dist),
                    (float)(cy2 - h * (Center2.X - Center1.X) / dist)),
                    new Vector2(
                    (float)(cx2 - h * (Center2.X - Center1.Y) / dist),
                    (float)(cy2 + h * (Center2.X - Center1.X) / dist))
                };

                // See if we have 1 or 2 solutions.
                if (dist == Radius1 + Radius2)
                {
                    list.Add(returner[0]);
                } else
                {
                    list.AddRange(returner);
                }
                return list;
            }

        }

        #endregion

        #region Serialize

        /// <summary>
        /// Serializes the Circle into a Dictionary.
        /// </summary>
        /// <returns>Serialized object</returns>
        public override MaxSerializedObject Serialize()
        {
            MaxSerializedObject data = MaxSerializedObject.Create();
            data.AddVector2(SER_POS, Center);
            data.AddFloat(SER_RADIUS, Radius);
            data.SetType(this);
            return data;
        }

        /// <summary>
        /// Deserializes a Dictionary into a Circle.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Center = Data.GetVector2(SER_POS);
            Radius = Data.GetFloat(SER_RADIUS);
        }

        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new Circle2D();
            a.Deserialize(Data);
            return a;
        }

        #endregion

    }
}
