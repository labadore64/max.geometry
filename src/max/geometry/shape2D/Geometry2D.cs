﻿using max.serialize;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace max.geometry.shape2D
{
    /// <summary>
    /// An abstract class representing common properties and methods for 2D Geometry.
    /// </summary>
    public abstract class Geometry2D : I2DGeometry
    {
        /// <summary>
        /// Whether or not this component is serialized when the parent's Serialize method is called.
        /// </summary>
        /// <value>True/False</value>
        public bool Serializable { get; set; }

        /// <summary>
        /// The center of the geometry.
        /// </summary>
        /// <value>Center</value>
        public virtual Vector2 Center { get; set; }
        /// <summary>
        /// The points of the geometry.
        /// </summary>
        /// <value>Array of points</value>
        public abstract Vector2[] Points { get; protected set; }

        /// <summary>
        /// Returns a copy of the current geometry.
        /// </summary>
        /// <returns>Copy of geometry</returns>
        public abstract I2DGeometry Copy();

        /// <summary>
        /// The points where a shape intersects this geometry.
        /// </summary>
        /// <param name="Shape">Intersecting shape</param>
        /// <returns>Intersection points</returns>
        public abstract List<Vector2> IntersectPoints(I2DGeometry Shape);

        /// <summary>
        /// Whether or not a point lies inside this geometry.
        /// </summary>
        /// <param name="Point">Point</param>
        /// <returns>True/False</returns>
        public abstract bool PointInGeometry(Vector2 Point);

        /// <summary>
        /// Serializes this object into MaxSerializedObject data.
        /// </summary>
        /// <returns>Data</returns>
        public abstract MaxSerializedObject Serialize();

        /// <summary>
        /// Deserializes MaxSerializedObject data.
        /// </summary>
        /// <param name="Data">Data</param>
        public abstract void Deserialize(MaxSerializedObject Data);

        /// <summary>
        /// Returns the closest Vector2 to a point from a list of Vector2.
        /// </summary>
        /// <param name="List">List</param>
        /// <param name="Point">Point to test</param>
        /// <returns>Closest point</returns>
        protected List<Vector2> GetClosestVector2(List<Vector2> List,Vector2 Point)
        {
            if (List.Count < 2)
            {
                return List;
            }

            Vector2 closestPoint = Vector2.Zero;
            List<Vector2> returner = new List<Vector2>();
            float dist = float.MaxValue;
            float testdist;

            for (int i = 0; i < List.Count; i++)
            {
                testdist = Vector2.Distance(Point, List[i]);
                if (testdist < dist)
                {
                    dist = testdist;
                    closestPoint = List[i];
                }
            }

            returner[0] = closestPoint;

            return returner;
        }

        public abstract IMaxSerializable CreateInstance(MaxSerializedObject Data);
    }
}
