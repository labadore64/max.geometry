﻿using Microsoft.Xna.Framework;

namespace max.geometry.helper
{
    /// <summary>
    /// Contains values used throughout geometry applications for Vector2.
    /// </summary>
    public class Vector3Helper
    {
        /// <summary>
        /// Represents an empty value. All coordinates are set to float.MaxValue.
        /// </summary>
        /// <value>Vector3(float.MaxValue, float.MaxValue, float.MaxValue)</value>
        public static readonly Vector3 DUMMY = new Vector3(float.MaxValue, float.MaxValue,float.MaxValue);
    }
}
