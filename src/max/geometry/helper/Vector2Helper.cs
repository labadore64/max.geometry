﻿using Microsoft.Xna.Framework;

namespace max.geometry.helper
{
    /// <summary>
    /// Contains values used throughout geometry applications for Vector2.
    /// </summary>
    public class Vector2Helper
    {
        /// <summary>
        /// Represents an empty value. All coordinates are set to float.MaxValue.
        /// </summary>
        /// <value>Vector2(float.MaxValue, float.MaxValue)</value>
        public static readonly Vector2 DUMMY = new Vector2(float.MaxValue, float.MaxValue);
    }
}
