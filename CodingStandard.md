Coding Standard
===========

This library's classes should be standardized according to the following specifications.

Parent Standards
-----------------
Please refer to the standards here (under construction) for global MaxLib standards.

Namespace
---------------
* All ``max.geometry`` classes must be under ``max.geometry`` namespace.

Commenting
---------------
* All methods should have clear commenting if they are longer than just a few lines.
* Every public method must have a summary, parameters and return values defined.

Inheritance
---------------
* All main geometry classes should implement ``IMaxSerializable`` (max.serializable).

Overloading
---------------
* Every method must be sectioned by using ``#region`` with the name of the method.
* Each method should pipe into a static method that only receives primative/Vector2/Vector3 parameters. This method should be able to be used without instantiating an object. This method should be on the bottom of the region. Only this method should have code; all other overloads should pipe to this method, if optimal.
* Arguments should be ordered as such: [Params for geometry object to test], [params for object to test against]
* Each method should be overloaded for the following conditions:
    * static methods that pass the object (or a representation of it as primatives) as a parameter (example: ``public static float Distance(GeometryObj Geometry, float Param, ...)``
        * Each method should pass Lines as type ``Line``, ``Vector2[]`` and ``Vector2, Vector2``
        * Each method should pass Polygons as type ``Polygon2D``, ``Vector2[]``
        * Each method should pass Circles as type ``Circle``, ``Vector2, float``
        * Each method that uses the object type as a parameter should call that instance's version of the non-static method (example: ``return Circle.Distance(...);``)
    * non-static methods
        * Each method should pass Lines as type ``Line``, ``Vector2[]`` and ``Vector2, Vector2``
        * Each method should pass Polygons as type ``Polygon2D``, ``Vector2[]``
        * Each method should pass Circles as type ``Circle``, ``Vector2, float``
        * Each method should try to pass to the base static method.

Serialization
------------------
* Strings used for keys must be consts.
* Serialization methods should always be last.