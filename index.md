# max.geometry

max.geometry is a MaxLib Helper Library that contains 2D geometry classes. It also has pathfinding capabilities.

* [Documentation](https://labadore64.gitlab.io/max.geometry/)
* [Source](https://gitlab.com/labadore64/max.geometry)