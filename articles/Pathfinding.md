# Pathfinding

Pathfinding is easy and only requires the following:

* A start and end point
* Collisions represented as an array of [Polygon2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Polygon2D.html)

First, create an instance of the PathFinder:

```
MapPathFinder PathFinder = new MapPathFinder(CollisionArray[]);
```

You should also set the room width and height properties. There are other options you can change as well if you feel the need to.

```
MapPathFinder.Options.Height = 600;
MapPathFinder.Options.Width = 800;
```

To get the path:

```
Path2D path = PathFinder.GetPathBetweenPoints(new Vector2(300,150),new Vector2(100,100));
```

Refer to [Path2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Path2D.html) for more information.