# Shapes

``max.geometry`` provides several 2D shapes out of the box to use in your projects. 

* [Circle2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Circle2D.html)
* [Line2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Line2D.html)
* [Polygon2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Polygon2D.html)
* [Path2D](https://labadore64.gitlab.io/max.geometry/api/max.geometry.shape2D.Path2D.html)

All geometry shapes implement the interface [I2DGeometry](https://labadore64.gitlab.io/max.geometry/api/max.geometry.I2DGeometry.html), which means they will all implement the following methods:

* Serialize()
* Deserialize()
* Copy()
* IntersectionPoints(I2DGeometry Shape)

To use a geometry shape as an object you can just create a new instance of one:
```
Circle2D Circle = new Circle2D(new Vector2(100, 100), 20);
Circle.PointOnCircle((float)(.5 * Math.PI));
```

You can also call static methods for calculating geometry from the type, if you don't need them as objects:

```
Circle2D.PointOnCircle(new Vector2(100, 50), 50, (float)(.5 * Math.PI));
```

Some methods can only be called from an instance though, keep in mind.