# Using max.geometry

``max.geometry`` is a library that has 2D shapes and pathfinding.

* [Installation](Installation.md)
* [Shapes](Shapes.md)
* [Pathfinding](Pathfinding.md)

## Max Dependencies:

* [max.serialize](https://labadore64.gitlab.io/max.serialize/)